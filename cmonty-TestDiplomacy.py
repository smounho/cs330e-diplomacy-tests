#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, army

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\n"
        x = diplomacy_read(s)
        test = army(["A", "Madrid", "Hold"])
        self.assertEqual(x.name, test.name)
        self.assertEqual(x.location, test.location)
        self.assertEqual(x.action, test.action)

    def test_read2(self):
        s = "B Barcelona Move Madrid\n"
        x = diplomacy_read(s)
        test = army(["B", "Barcelona", "Move", "Madrid"])
        self.assertEqual(x.name, test.name)
        self.assertEqual(x.location, test.location)
        self.assertEqual(x.action, test.action)
        self.assertEqual(x.acting_on, test.acting_on)

    def test_read3(self):
        s = "C London Support B\n"
        x = diplomacy_read(s)
        test = army(["C", "London", "Support", "B"])
        self.assertEqual(x.name, test.name)
        self.assertEqual(x.location, test.location)
        self.assertEqual(x.action, test.action)
        self.assertEqual(x.acting_on, test.acting_on)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        army1 = army(["A", "Madrid", "Hold"])
        army2 = army(["B", "Barcelona", "Move", "Madrid"])
        army3 = army(["C", "London", "Support", "B"])
        start_test = [army1, army2, army3]

        test_result = diplomacy_eval(start_test)

        self.assertEqual("[dead]", test_result[0].location)
        self.assertEqual("Madrid", test_result[1].location)
        self.assertEqual("London", test_result[2].location)

    def test_eval_2(self):
        army1 = army(["A", "Madrid", "Hold"])
        army2 = army(["B", "Barcelona", "Move", "Madrid"])
        start_test = [army1, army2]

        test_result = diplomacy_eval(start_test)

        self.assertEqual("[dead]", test_result[0].location)
        self.assertEqual("[dead]", test_result[1].location)

    def test_eval_3(self):
        army1 = army(["A", "Madrid", "Hold"])
        army2 = army(["B", "Barcelona", "Move", "Madrid"])
        army3 = army(["C", "London", "Support", "B"])
        army4 = army(["D", "Austin", "Move", "London"])
        start_test = [army1, army2, army3, army4]

        test_result = diplomacy_eval(start_test)

        self.assertEqual("[dead]", test_result[0].location)
        self.assertEqual("[dead]", test_result[1].location)
        self.assertEqual("[dead]", test_result[2].location)
        self.assertEqual("[dead]", test_result[3].location)

    def test_eval_4(self):
        army1 = army(["A", "Austin", "Hold"])
        army2 = army(["B", "Madrid", "Move", "Austin"])
        army3 = army(["C", "London", "Support", "A"])
        army4 = army(["D", "NewYork", "Support", "B"])
        army5 = army(["E", "Spain", "Move", "Austin"])
        start_test = [army1, army2, army3, army4, army5]

        test_result = diplomacy_eval(start_test)

        self.assertEqual("[dead]", test_result[0].location)
        self.assertEqual("[dead]", test_result[1].location)
        self.assertEqual("London", test_result[2].location)
        self.assertEqual("NewYork", test_result[3].location)
        self.assertEqual("[dead]", test_result[4].location)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        army1 = army(["A", "Madrid", "Hold"])
        diplomacy_print(w, army1)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print1(self):
        w = StringIO()
        army2 = army(["B", "Barcelona", "Move", "Madrid"])
        diplomacy_print(w, army2)
        self.assertEqual(w.getvalue(), "B Madrid\n")

    def test_print2(self):
        w = StringIO()
        army3 = army(["C", "London", "Support", "B"])
        diplomacy_print(w, army3)
        self.assertEqual(w.getvalue(), "C London\n")

    # -----
    # solve
    # -----
    #
    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

#pragma: no cover
